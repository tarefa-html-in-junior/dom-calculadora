const input = document.getElementById('inputValue');
const addButton = document.querySelector('button');
const valuesList = document.getElementById('valuesList');
const averageValue = document.getElementById('averageValue');

let values = [];

function updateAverage() {
    const sum = values.reduce((acc, value) => acc + value, 0);
    const avg = sum / values.length;
    averageValue.textContent = avg.toFixed(2);
}

function addValue() {
    const value = parseFloat(input.value);
    if (!isNaN(value)) {
        values.push(value);
        input.value = '';
        updateValuesList();
        updateAverage();
    }
}

function updateValuesList() {
    valuesList.innerHTML = '<strong>Valores adicionados:</strong><br>';
    values.forEach(value => {
        valuesList.innerHTML += value + '<br>';
    });
}

addButton.addEventListener('click', addValue);
